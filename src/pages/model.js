import React, {useEffect, useState} from "react";
import {motion, useViewportScroll, useTransform} from "framer-motion";

//Components
//Ease
const transition = {duration: 1.4, ease: [0.6, 0.01, -0.05, 0.9]};

const firstName = {
	initial: {
		y: 0,
	},
	animate: {
		y: 0,
		transition: {
			delayChildren: 0.6,
			staggerChildren: 0.04,
			staggerDirection: -1,
		},
	},
};

const lastName = {
	initial: {
		y: 0,
	},
	animate: {
		y: 0,
		transition: {
			delayChildren: 0.6,
			staggerChildren: 0.04,
			staggerDirection: 1,
		},
	},
};

const letter = {
	initial: {
		y: 400,
	},
	animate: {
		y: 0,
		transition: {duration: 1, ...transition},
	},
};

const Model = ({imageDetails}) => {
	const {scrollYProgress} = useViewportScroll();
	const scale = useTransform(scrollYProgress, [0, 1], [1, 1.15]);

	const [canScroll, setCanScroll] = useState(false);

	useEffect(() => {
		if (canScroll === false) {
			document.querySelector("body").classList.add("no-scroll");
		} else {
			document.querySelector("body").classList.remove("no-scroll");
		}
	}, [canScroll]);

	return (
		<div>
			<motion.div
				onAnimationComplete={() => setCanScroll(true)}
				className="single"
				initial="initial"
				animate="animate"
				exit="exit"
			>
				<div className="container fluid">
					<div className="row center top-row">
						<div className="top">
							<motion.div
								initial={{opacity: 0, y: 20}}
								animate={{
									opacity: 1,
									y: 0,
									transition: {delay: 1.2, ...transition},
								}}
								className="details"
							>
								<div className="location">
									<span>INTRODUCTION</span>
								</div>
								<div className="mua">MUA: @mylifeascrystall</div>
							</motion.div>
							<motion.div className="model">
								<motion.span className="first" variants={firstName}>
									<motion.span variants={letter}>L'</motion.span>
									<motion.span variants={letter}>e</motion.span>
									<motion.span variants={letter}>v</motion.span>
									<motion.span variants={letter}>o</motion.span>
									<motion.span variants={letter}>l</motion.span>
									<motion.span variants={letter}>u</motion.span>
									<motion.span variants={letter}>t</motion.span>
									<motion.span variants={letter}>i</motion.span>
									<motion.span variants={letter}>o</motion.span>
									<motion.span variants={letter}>n</motion.span>
								</motion.span>
								<motion.span className="last" variants={lastName}>
									<motion.span variants={letter}>D'</motion.span>
									<motion.span variants={letter}>e</motion.span>
									<motion.span variants={letter}>n</motion.span>
									<motion.span variants={letter}>t</motion.span>
									<motion.span variants={letter}>r</motion.span>
									<motion.span variants={letter}>e</motion.span>
									<motion.span variants={letter}>p</motion.span>
									<motion.span variants={letter}>r</motion.span>
									<motion.span variants={letter}>i</motion.span>
									<motion.span variants={letter}>s</motion.span>
									<motion.span variants={letter}>e</motion.span>
								</motion.span>
							</motion.div>
							<motion.div className="model2">
								<motion.span className="f2" variants={lastName}>
									<motion.span variants={letter}>D</motion.span>
									<motion.span variants={letter}>e</motion.span>
									<motion.span variants={letter}>&nbsp;</motion.span>
									<motion.span variants={letter}>c</motion.span>
									<motion.span variants={letter}>l</motion.span>
									<motion.span variants={letter}>a</motion.span>
									<motion.span variants={letter}>s</motion.span>
									<motion.span variants={letter}>s</motion.span>
									<motion.span variants={letter}>i</motion.span>
									<motion.span variants={letter}>q</motion.span>
									<motion.span variants={letter}>u</motion.span>
									<motion.span variants={letter}>e</motion.span>
									<motion.span variants={letter}>&nbsp;</motion.span>
									<motion.span variants={letter}>J'</motion.span>
									<motion.span variants={letter}>u</motion.span>
									<motion.span variants={letter}>s</motion.span>
									<motion.span variants={letter}>q</motion.span>
									<motion.span variants={letter}>u'</motion.span>
									<motion.span variants={letter}>a</motion.span>
									<motion.span variants={letter}>&nbsp;</motion.span>
									<motion.span variants={letter}>{"4"}</motion.span>
									<motion.span variants={letter}>{"."}</motion.span>
									<motion.span variants={letter}>{"0"}</motion.span>
								</motion.span>
							</motion.div>
						</div>
					</div>
					<div className="row bottom-row">
						<div className="bottom">
							<motion.div className="image-container-single">
								<motion.div
									initial={{
										y: "-50%",
										width: imageDetails.width,
										height: imageDetails.height,
									}}
									animate={{
										y: 0,
										width: "100%",
										height: window.innerWidth > 1440 ? 800 : 400,
										transition: {delay: 0.2, ...transition},
									}}
									className="thumbnail-single"
								>
									<motion.div
										className="frame-single"
										whileHover="hover"
										transition={transition}
									>
										<motion.img
											src={require("../images/yasmeen.webp")}
											alt="an image"
											style={{scale: scale}}
											initial={{scale: 1.0}}
											animate={{
												transition: {delay: 0.2, ...transition},
												y: window.innerWidth > 1440 ? -1200 : -600,
											}}
										/>
									</motion.div>
								</motion.div>
							</motion.div>
						</div>
					</div>
				</div>
				<div className="detailed-information">
					<div className="container">
						<div className="row">
							<h2 className="title">
								The insiration behind the artwork & <br /> what it means.
							</h2>
							<p className="text-left">
								"L'entreprise classique, souvent désignée comme une organisation
								traditionnelle, est caractérisée par une structure hiérarchique
								rigide, des processus manuels et une communication verticale.
								Elle se distingue par une gestion centralisée et des chaînes de
								commandement formelles. Les opérations sont souvent exécutées
								manuellement, ce qui peut entraîner des flux de travail moins
								efficients. De plus, les entreprises classiques peuvent parfois
								faire preuve de résistance face aux changements et aux
								innovations. Cette approche, bien qu'ancrée dans la tradition,
								évolue face aux défis de l'économie moderne, conduisant à
								l'émergence de nouvelles formes d'organisation plus agiles et
								technologiquement avancées."
							</p>
						</div>
					</div>
				</div>
			</motion.div>
		</div>
	);
};

export default Model;
